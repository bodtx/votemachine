# Machine à voter

Basé sur https://github.com/lexus2k/lcdgfx
Machine à voter 4 boutons

fonctions:

 * 1 bouton = 1 vote
 * affichage du nombre de votes
 * combinaison des 2 premiers boutons pour afficher le résultat

## Demo

1. **[Code demo wokwiki](https://wokwi.com/projects/353737162254514177)**


2. ![](https://files.mastodon.social/media_attachments/files/109/680/900/910/639/527/original/0deb02a18b2ff645.mp4)
